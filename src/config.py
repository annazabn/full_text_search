APP = {'name': 'full_text_search', 'port': 5588, 'host': '0.0.0.0'}

DOCUMENTS_DIR = '/app/files'

LOG_DIR = '/app/log'
LOG_FILE = 'search.log'
LOG_FORMAT = '%(asctime)s %(levelname)s | %(name)s %(message)s'
