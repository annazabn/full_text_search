FROM python:3.7

RUN apt-get update

COPY ./requirements.txt ./
RUN pip install --upgrade pip
RUN pip install -r requirements.txt && rm requirements.txt

COPY ./src /app/src
WORKDIR /app
ENV PYTHONPATH "${PYTHONPATH}:/app"

CMD ["python3", "src/app.py"]
